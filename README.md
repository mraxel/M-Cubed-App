# M-Cubed

## End Of Life (Deprecation Notice)
Hello. This project was originally created with the intention of being the
real-world problem that creating an app could solve, for the purpose of providing
a realistic list of requirements to use when teaching myself how to create an
android app. This was back when I was still in secondary school.

Since then I've started an engineering degree, learnt `C` and a whole bunch
of microcontroller platforms, and generally moved towards more interesting
hardware projects and away from web- and app-design projects.

This app is now 4 version behind the latest version of Android, has many bugs
that were never addressed, and is of the general code-quality of a self-taught
school student. With that in mind, coupled with the fact that I haven't touched
it in many years, I'm archiving the repository. If you somehow managed to find
this project and found it useful you are more than welcome to fork and continue
the work, but my ability and interest in the project has waned significanantly
and I will no longer be even pretending that I will one-day have the time to
update it properly.

-Axel  
2020-07-23  

## What it is?
M-Cubed is a handy tool for Adjudicators. It is a calculator designed to
specifically tally debating scores, according to the [Australia-Asia Debating
Guide](http://dav.com.au/resources/aadg.php) (AADG). It also has some other
handy features, like a standards tab that lists average scores for each category
to make it easy to check that your scores are valid.

## What's in a name?
Debates are scored based on three metrics: Matter, Method & Manner. The three
M's of debating. 3M is already a registered company. M3 is a
standard size metric nut-bolt combination, so that leaves M-Cubed (we don't use
the number 3 because it's too hard to get superscript in Markdown, URLs, Google
Play Store listings, other places the name needs to appear, etc.).

## Any planned features?
This section has two components. The first: Yes, I plan on overhauling how
scores are entered into the app, to make the app able to work with
individual-first as well as overall-score-first methodologies of scoring. (or,
to use buzzwords, "Improve the UX design for our target users"). I also plan to
include, at some point, a speech timer, with all the usual bells and whistles
suitable for debating (vibrate only to not distract people, presets with the
speaker times, multiple buzzes per timer, etc.). The second part of this
section can be answered "No, I don't have time and probably won't have the time
for a long time to come".

## Requirements?
Currently the Minimum Target SDK level is Android SDK 19 (which is Android
Kit-Kat for those who aren't Android developers), so any phone (or tablet)
running Kit-Kat or later *should* work.

## Why is it crap?
Good question. I wrote this over the course of a few years, having never touched
Java/Android development before, as a "learning project", one of
those things you write when you are learning a new language. One day I should
fix it up. Today is not that day. (ATT. FUTURE SELF: I am sorry, and please
remove this section once you've finished).

## Any help/support available?
No, you're on your own. Sorry.

## Can I get this from the Google Play Store?
No. I don't really have $25 sitting around to pay the fees to put a half-baked,
half-finished & half-buggy app out there for all the world to download and
complain about. If I ever get this in a properly functioning state then I'll
revisit this.

## Is there an iOS version?
I haven't spent $25 for a Google Play Store listing, what makes you think I have
the dosh for a $99 Apple Developers License?

## License?
This is not beerware, I don't really drink beer. If anything this is Milkware.
I'm basically happy for you to use this for whatever, as long as it **never**
comes back to me.
