package com.independent.axel.m-cubed;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import layout.ScoresFragment;
import layout.SpeakersFragment;
import layout.StandardsFragment;

public class MainActivity extends AppCompatActivity{

    // This is used to track if the app is newly opened, to set the first fragment (otherwise it's just a white screen with bottom toolbar
    // It's also inverted, i.e.: firstRun = false means it is the first run, and firstRun returns true for every following run.
    private boolean firstRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupNavigationView();
    }

    // onConfigurationChanged is called when the orientation changes. We use this to set the flag so that the fragment doesn't reset back to 0 (Speakers_Fragment)
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        firstRun = false;
    }

    private void setupNavigationView() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        if (bottomNavigationView != null) {
            if(!firstRun) {
                Menu menu = bottomNavigationView.getMenu();
                selectFragment(menu.getItem(0));
                firstRun = true;
            }
            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        selectFragment(item);
                        return false;
                    }
                }
            );
        }
    }

    //Perform action when any item is selected.
    //@param item Item that is selected.
    protected void selectFragment(MenuItem item) {
        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.navigation_speakers:
                // Action to perform when Speakers Menu item is selected.
                pushFragment(new SpeakersFragment());
                break;
            case R.id.navigation_scores:
                // Action to perform when Scores Menu item is selected.
                pushFragment(new ScoresFragment());
                break;
            case R.id.navigation_standards:
                // Action to perform when Standards Menu item is selected.
                pushFragment(new StandardsFragment());
                break;
        }
    }
    //Method to push any fragment into given id.
    //@param fragment An instance of Fragment to show into the given id.
    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (fragmentTransaction != null) {
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.commit();
            }
        }
    }
}
