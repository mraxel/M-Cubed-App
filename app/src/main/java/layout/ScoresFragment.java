package layout;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.independent.axel.dav.R;

public class ScoresFragment extends Fragment {

    // In case it isn't obvious, we need to make sure that when I say "TextView", java knows that we are talking about viewable text.
    public TextView FirstAffMatterScore;
    public TextView FirstAffMethodScore;
    public TextView FirstAffMannerScore;
    public TextView FirstAffTotalScore;
    public TextView FirstNegMatterScore;
    public TextView FirstNegMethodScore;
    public TextView FirstNegMannerScore;
    public TextView FirstNegTotalScore;
    public TextView SecondAffMatterScore;
    public TextView SecondAffMethodScore;
    public TextView SecondAffMannerScore;
    public TextView SecondAffTotalScore;
    public TextView SecondNegMatterScore;
    public TextView SecondNegMethodScore;
    public TextView SecondNegMannerScore;
    public TextView SecondNegTotalScore;
    public TextView ThirdAffMatterScore;
    public TextView ThirdAffMethodScore;
    public TextView ThirdAffMannerScore;
    public TextView ThirdAffTotalScore;
    public TextView ThirdNegMatterScore;
    public TextView ThirdNegMethodScore;
    public TextView ThirdNegMannerScore;
    public TextView ThirdNegTotalScore;
    public TextView AffirmativeMatterScore;
    public TextView AffirmativeMethodScore;
    public TextView AffirmativeMannerScore;
    public TextView NegativeMatterScore;
    public TextView NegativeMethodScore;
    public TextView NegativeMannerScore;
    public TextView AffirmativeScore;
    public TextView NegativeScore;
    public TextView Results;

    // Don't forget the token button.
    public Button ResetButton;

    int FirstAffMatterValue;
    int FirstAffMethodValue;
    int FirstAffMannerValue;
    int FirstAffTotalValue;
    int FirstNegMatterValue;
    int FirstNegMethodValue;
    int FirstNegMannerValue;
    int FirstNegTotalValue;
    int SecondAffMatterValue;
    int SecondAffMethodValue;
    int SecondAffMannerValue;
    int SecondAffTotalValue;
    int SecondNegMatterValue;
    int SecondNegMethodValue;
    int SecondNegMannerValue;
    int SecondNegTotalValue;
    int ThirdAffMatterValue;
    int ThirdAffMethodValue;
    int ThirdAffMannerValue;
    int ThirdAffTotalValue;
    int ThirdNegMatterValue;
    int ThirdNegMethodValue;
    int ThirdNegMannerValue;
    int ThirdNegTotalValue;
    int AffirmativeMatterTotal;
    int AffirmativeMethodTotal;
    int AffirmativeMannerTotal;
    int AffirmativeTotal;
    int NegativeMatterTotal;
    int NegativeMethodTotal;
    int NegativeMannerTotal;
    int NegativeTotal;
    int Margin;
    int AbsMargin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // This is needed for something important, I'm not sure what, but everything crashes and burns without it, so don't remove it.
        View rootView = inflater.inflate(R.layout.fragment_scores, container, false);

        // Access preference storage
        SharedPreferences sharedPref = getActivity().getSharedPreferences("com.independent.dav.scores", Context.MODE_PRIVATE);

        // Assign string variables to the cells they represent so that everything can be displayed properly... At least I think that's what this does...
        FirstAffMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB2);
        FirstAffMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC2);
        FirstAffMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD2);
        FirstAffTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE2);
        FirstNegMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB7);
        FirstNegMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC7);
        FirstNegMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD7);
        FirstNegTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE7);
        SecondAffMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB3);
        SecondAffMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC3);
        SecondAffMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD3);
        SecondAffTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE3);
        SecondNegMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB8);
        SecondNegMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC8);
        SecondNegMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD8);
        SecondNegTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE8);
        ThirdAffMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB4);
        ThirdAffMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC4);
        ThirdAffMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD4);
        ThirdAffTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE4);
        ThirdNegMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB9);
        ThirdNegMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC9);
        ThirdNegMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD9);
        ThirdNegTotalScore = (TextView) rootView.findViewById(R.id.textViewCellE9);
        AffirmativeMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB5);
        AffirmativeMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC5);
        AffirmativeMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD5);
        NegativeMatterScore = (TextView) rootView.findViewById(R.id.textViewCellB10);
        NegativeMethodScore = (TextView) rootView.findViewById(R.id.textViewCellC10);
        NegativeMannerScore = (TextView) rootView.findViewById(R.id.textViewCellD10);
        AffirmativeScore = (TextView) rootView.findViewById(R.id.textViewCellE5);
        NegativeScore = (TextView) rootView.findViewById(R.id.textViewCellE10);
        Results = (TextView) rootView.findViewById(R.id.results);

        // Get the saved values that we hid in SharedPreferences back in the SpeakersFragment, and add the average value to them to represent the real scores.
        FirstAffMatterValue = sharedPref.getInt("FirstAffMatterStorage", 0) + 30;
        FirstAffMethodValue = sharedPref.getInt("FirstAffMethodStorage", 0) + 15;
        FirstAffMannerValue = sharedPref.getInt("FirstAffMannerStorage", 0) + 30;
        FirstNegMatterValue = sharedPref.getInt("FirstNegMatterStorage", 0) + 30;
        FirstNegMethodValue = sharedPref.getInt("FirstNegMethodStorage", 0) + 15;
        FirstNegMannerValue = sharedPref.getInt("FirstNegMannerStorage", 0) + 30;
        SecondAffMatterValue = sharedPref.getInt("SecondAffMatterStorage", 0) + 30;
        SecondAffMethodValue = sharedPref.getInt("SecondAffMethodStorage", 0) + 15;
        SecondAffMannerValue = sharedPref.getInt("SecondAffMannerStorage", 0) + 30;
        SecondNegMatterValue = sharedPref.getInt("SecondNegMatterStorage", 0) + 30;
        SecondNegMethodValue = sharedPref.getInt("SecondNegMethodStorage", 0) + 15;
        SecondNegMannerValue = sharedPref.getInt("SecondNegMannerStorage", 0) + 30;
        ThirdAffMatterValue = sharedPref.getInt("ThirdAffMatterStorage", 0) + 30;
        ThirdAffMethodValue = sharedPref.getInt("ThirdAffMethodStorage", 0) + 15;
        ThirdAffMannerValue = sharedPref.getInt("ThirdAffMannerStorage", 0) + 30;
        ThirdNegMatterValue = sharedPref.getInt("ThirdNegMatterStorage", 0) + 30;
        ThirdNegMethodValue = sharedPref.getInt("ThirdNegMethodStorage", 0) + 15;
        ThirdNegMannerValue = sharedPref.getInt("ThirdNegMannerStorage", 0) + 30;
        FirstAffTotalValue = FirstAffMatterValue + FirstAffMethodValue + FirstAffMannerValue;
        FirstNegTotalValue = FirstNegMatterValue + FirstNegMethodValue + FirstNegMannerValue;
        SecondAffTotalValue = SecondAffMatterValue + SecondAffMethodValue + SecondAffMannerValue;
        SecondNegTotalValue = SecondNegMatterValue + SecondNegMethodValue + SecondNegMannerValue;
        ThirdAffTotalValue = ThirdAffMatterValue + ThirdAffMethodValue + ThirdAffMannerValue;
        ThirdNegTotalValue = ThirdNegMatterValue + ThirdNegMethodValue + ThirdNegMannerValue;
        AffirmativeMatterTotal = FirstAffMatterValue + SecondAffMatterValue + ThirdAffMatterValue;
        AffirmativeMethodTotal = FirstAffMethodValue + SecondAffMethodValue + ThirdAffMethodValue;
        AffirmativeMannerTotal = FirstAffMannerValue + SecondAffMannerValue + ThirdAffMannerValue;
        NegativeMatterTotal = FirstNegMatterValue + SecondNegMatterValue + ThirdNegMatterValue;
        NegativeMethodTotal = FirstNegMethodValue + SecondNegMethodValue + ThirdNegMethodValue;
        NegativeMannerTotal = FirstNegMannerValue + SecondNegMannerValue + ThirdNegMannerValue;
        AffirmativeTotal = FirstAffTotalValue + SecondAffTotalValue + ThirdAffTotalValue;
        NegativeTotal = FirstNegTotalValue + SecondNegTotalValue + ThirdNegTotalValue;
        Margin = AffirmativeTotal - NegativeTotal;
        AbsMargin = NegativeTotal - AffirmativeTotal;

        // Display the calculated scores on the screen, in the table, on the score-sheet.
        FirstAffMatterScore.setText(String.valueOf(FirstAffMatterValue));
        FirstAffMethodScore.setText(String.valueOf(FirstAffMethodValue));
        FirstAffMannerScore.setText(String.valueOf(FirstAffMannerValue));
        FirstAffTotalScore.setText(String.valueOf(FirstAffTotalValue));
        FirstNegMatterScore.setText(String.valueOf(FirstNegMatterValue));
        FirstNegMethodScore.setText(String.valueOf(FirstNegMethodValue));
        FirstNegMannerScore.setText(String.valueOf(FirstNegMannerValue));
        FirstNegTotalScore.setText(String.valueOf(FirstNegTotalValue));
        SecondAffMatterScore.setText(String.valueOf(SecondAffMatterValue));
        SecondAffMethodScore.setText(String.valueOf(SecondAffMethodValue));
        SecondAffMannerScore.setText(String.valueOf(SecondAffMannerValue));
        SecondAffTotalScore.setText(String.valueOf(SecondAffTotalValue));
        SecondNegMatterScore.setText(String.valueOf(SecondNegMatterValue));
        SecondNegMethodScore.setText(String.valueOf(SecondNegMethodValue));
        SecondNegMannerScore.setText(String.valueOf(SecondNegMannerValue));
        SecondNegTotalScore.setText(String.valueOf(SecondNegTotalValue));
        ThirdAffMatterScore.setText(String.valueOf(ThirdAffMatterValue));
        ThirdAffMethodScore.setText(String.valueOf(ThirdAffMethodValue));
        ThirdAffMannerScore.setText(String.valueOf(ThirdAffMannerValue));
        ThirdAffTotalScore.setText(String.valueOf(ThirdAffTotalValue));
        ThirdNegMatterScore.setText(String.valueOf(ThirdNegMatterValue));
        ThirdNegMethodScore.setText(String.valueOf(ThirdNegMethodValue));
        ThirdNegMannerScore.setText(String.valueOf(ThirdNegMannerValue));
        ThirdNegTotalScore.setText(String.valueOf(ThirdNegTotalValue));
        AffirmativeMatterScore.setText(String.valueOf(AffirmativeMatterTotal));
        AffirmativeMethodScore.setText(String.valueOf(AffirmativeMethodTotal));
        AffirmativeMannerScore.setText(String.valueOf(AffirmativeMannerTotal));
        NegativeMatterScore.setText(String.valueOf(NegativeMatterTotal));
        NegativeMethodScore.setText(String.valueOf(NegativeMethodTotal));
        NegativeMannerScore.setText(String.valueOf(NegativeMannerTotal));
        AffirmativeScore.setText(String.valueOf(AffirmativeTotal));
        NegativeScore.setText(String.valueOf(NegativeTotal));

        // Spell out the winning team, and the margin by which they won. Alternatively, warn that a draw has occurred.
        if(Margin == 0) {
            Results.setText(R.string.result_draw);
        } else if (Margin == 1) {
            Results.setText(getResources().getString(R.string.result_affirmative) + Margin + getResources().getString(R.string.result_point));
        } else if (Margin > 0) {
            Results.setText(getResources().getString(R.string.result_affirmative) + Margin + getResources().getString(R.string.result_points));
        } else if (Margin == -1) {
            Results.setText(getResources().getString(R.string.result_negative) + AbsMargin + getResources().getString(R.string.result_point));
        } else if (Margin < 0) {
            Results.setText(getResources().getString(R.string.result_negative) + AbsMargin + getResources().getString(R.string.result_points));
        } else {
            Results.setText(R.string.error);
        }

        // Set all scores to zero in SpeakersFragment if the "Reset" button is pressed.
        ResetButton = (Button) rootView.findViewById(R.id.resetButton);
        ResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                // Open edit session with SharedPreferences
                SharedPreferences sharedPref = getActivity().getSharedPreferences("com.independent.dav.scores", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                // Set every value to zip.
                editor.putInt("FirstAffMatterStorage", 0);
                editor.putInt("FirstAffMethodStorage", 0);
                editor.putInt("FirstAffMannerStorage", 0);
                editor.putInt("FirstNegMatterStorage", 0);
                editor.putInt("FirstNegMethodStorage", 0);
                editor.putInt("FirstNegMannerStorage", 0);
                editor.putInt("SecondAffMatterStorage", 0);
                editor.putInt("SecondAffMethodStorage", 0);
                editor.putInt("SecondAffMannerStorage", 0);
                editor.putInt("SecondNegMatterStorage", 0);
                editor.putInt("SecondNegMethodStorage", 0);
                editor.putInt("SecondNegMannerStorage", 0);
                editor.putInt("ThirdAffMatterStorage", 0);
                editor.putInt("ThirdAffMethodStorage", 0);
                editor.putInt("ThirdAffMannerStorage", 0);
                editor.putInt("ThirdNegMatterStorage", 0);
                editor.putInt("ThirdNegMethodStorage", 0);
                editor.putInt("ThirdNegMannerStorage", 0);
                // Save changes
                editor.apply();

                // Copy new values to score-sheet
                FirstAffMatterValue = sharedPref.getInt("FirstAffMatterStorage", 0) + 30;
                FirstAffMethodValue = sharedPref.getInt("FirstAffMethodStorage", 0) + 15;
                FirstAffMannerValue = sharedPref.getInt("FirstAffMannerStorage", 0) + 30;
                FirstNegMatterValue = sharedPref.getInt("FirstNegMatterStorage", 0) + 30;
                FirstNegMethodValue = sharedPref.getInt("FirstNegMethodStorage", 0) + 15;
                FirstNegMannerValue = sharedPref.getInt("FirstNegMannerStorage", 0) + 30;
                SecondAffMatterValue = sharedPref.getInt("SecondAffMatterStorage", 0) + 30;
                SecondAffMethodValue = sharedPref.getInt("SecondAffMethodStorage", 0) + 15;
                SecondAffMannerValue = sharedPref.getInt("SecondAffMannerStorage", 0) + 30;
                SecondNegMatterValue = sharedPref.getInt("SecondNegMatterStorage", 0) + 30;
                SecondNegMethodValue = sharedPref.getInt("SecondNegMethodStorage", 0) + 15;
                SecondNegMannerValue = sharedPref.getInt("SecondNegMannerStorage", 0) + 30;
                ThirdAffMatterValue = sharedPref.getInt("ThirdAffMatterStorage", 0) + 30;
                ThirdAffMethodValue = sharedPref.getInt("ThirdAffMethodStorage", 0) + 15;
                ThirdAffMannerValue = sharedPref.getInt("ThirdAffMannerStorage", 0) + 30;
                ThirdNegMatterValue = sharedPref.getInt("ThirdNegMatterStorage", 0) + 30;
                ThirdNegMethodValue = sharedPref.getInt("ThirdNegMethodStorage", 0) + 15;
                ThirdNegMannerValue = sharedPref.getInt("ThirdNegMannerStorage", 0) + 30;
                FirstAffTotalValue = FirstAffMatterValue + FirstAffMethodValue + FirstAffMannerValue;
                FirstNegTotalValue = FirstNegMatterValue + FirstNegMethodValue + FirstNegMannerValue;
                SecondAffTotalValue = SecondAffMatterValue + SecondAffMethodValue + SecondAffMannerValue;
                SecondNegTotalValue = SecondNegMatterValue + SecondNegMethodValue + SecondNegMannerValue;
                ThirdAffTotalValue = ThirdAffMatterValue + ThirdAffMethodValue + ThirdAffMannerValue;
                ThirdNegTotalValue = ThirdNegMatterValue + ThirdNegMethodValue + ThirdNegMannerValue;
                AffirmativeMatterTotal = FirstAffMatterValue + SecondAffMatterValue + ThirdAffMatterValue;
                AffirmativeMethodTotal = FirstAffMethodValue + SecondAffMethodValue + ThirdAffMethodValue;
                AffirmativeMannerTotal = FirstAffMannerValue + SecondAffMannerValue + ThirdAffMannerValue;
                NegativeMatterTotal = FirstNegMatterValue + SecondNegMatterValue + ThirdNegMatterValue;
                NegativeMethodTotal = FirstNegMethodValue + SecondNegMethodValue + ThirdNegMethodValue;
                NegativeMannerTotal = FirstNegMannerValue + SecondNegMannerValue + ThirdNegMannerValue;
                AffirmativeTotal = FirstAffTotalValue + SecondAffTotalValue + ThirdAffTotalValue;
                NegativeTotal = FirstNegTotalValue + SecondNegTotalValue + ThirdNegTotalValue;
                Margin = AffirmativeTotal - NegativeTotal;
                AbsMargin = NegativeTotal - AffirmativeTotal;
                FirstAffMatterScore.setText(String.valueOf(FirstAffMatterValue));
                FirstAffMethodScore.setText(String.valueOf(FirstAffMethodValue));
                FirstAffMannerScore.setText(String.valueOf(FirstAffMannerValue));
                FirstAffTotalScore.setText(String.valueOf(FirstAffTotalValue));
                FirstNegMatterScore.setText(String.valueOf(FirstNegMatterValue));
                FirstNegMethodScore.setText(String.valueOf(FirstNegMethodValue));
                FirstNegMannerScore.setText(String.valueOf(FirstNegMannerValue));
                FirstNegTotalScore.setText(String.valueOf(FirstNegTotalValue));
                SecondAffMatterScore.setText(String.valueOf(SecondAffMatterValue));
                SecondAffMethodScore.setText(String.valueOf(SecondAffMethodValue));
                SecondAffMannerScore.setText(String.valueOf(SecondAffMannerValue));
                SecondAffTotalScore.setText(String.valueOf(SecondAffTotalValue));
                SecondNegMatterScore.setText(String.valueOf(SecondNegMatterValue));
                SecondNegMethodScore.setText(String.valueOf(SecondNegMethodValue));
                SecondNegMannerScore.setText(String.valueOf(SecondNegMannerValue));
                SecondNegTotalScore.setText(String.valueOf(SecondNegTotalValue));
                ThirdAffMatterScore.setText(String.valueOf(ThirdAffMatterValue));
                ThirdAffMethodScore.setText(String.valueOf(ThirdAffMethodValue));
                ThirdAffMannerScore.setText(String.valueOf(ThirdAffMannerValue));
                ThirdAffTotalScore.setText(String.valueOf(ThirdAffTotalValue));
                ThirdNegMatterScore.setText(String.valueOf(ThirdNegMatterValue));
                ThirdNegMethodScore.setText(String.valueOf(ThirdNegMethodValue));
                ThirdNegMannerScore.setText(String.valueOf(ThirdNegMannerValue));
                ThirdNegTotalScore.setText(String.valueOf(ThirdNegTotalValue));
                AffirmativeMatterScore.setText(String.valueOf(AffirmativeMatterTotal));
                AffirmativeMethodScore.setText(String.valueOf(AffirmativeMethodTotal));
                AffirmativeMannerScore.setText(String.valueOf(AffirmativeMannerTotal));
                NegativeMatterScore.setText(String.valueOf(NegativeMatterTotal));
                NegativeMethodScore.setText(String.valueOf(NegativeMethodTotal));
                NegativeMannerScore.setText(String.valueOf(NegativeMannerTotal));
                AffirmativeScore.setText(String.valueOf(AffirmativeTotal));
                NegativeScore.setText(String.valueOf(NegativeTotal));
                if(Margin == 0) {
                    Results.setText(R.string.result_draw);
                } else if (Margin == 1) {
                    Results.setText(getResources().getString(R.string.result_affirmative) + Margin + getResources().getString(R.string.result_point));
                } else if (Margin > 0) {
                    Results.setText(getResources().getString(R.string.result_affirmative) + Margin + getResources().getString(R.string.result_points));
                } else if (Margin == -1) {
                    Results.setText(getResources().getString(R.string.result_negative) + AbsMargin + getResources().getString(R.string.result_point));
                } else if (Margin < 0) {
                    Results.setText(getResources().getString(R.string.result_negative) + AbsMargin + getResources().getString(R.string.result_points));
                } else {
                    Results.setText(R.string.error);
                }
            }
        });

        // Used to cause all the changing of the text values to actually take effect
        return rootView;
    }
}
