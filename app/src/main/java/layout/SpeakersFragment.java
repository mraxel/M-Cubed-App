package layout;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.independent.axel.dav.R;

public class SpeakersFragment extends Fragment {

    // Declare stuff in memory so that Java and I are on the same page about what we are talking about

    public static String PackageName;

    public ImageView Output;

    public Button FirstAffMatterMore;
    public Button FirstAffMatterLess;
    public Button FirstAffMethodMore;
    public Button FirstAffMethodLess;
    public Button FirstAffMannerMore;
    public Button FirstAffMannerLess;
    public Button FirstNegMatterMore;
    public Button FirstNegMatterLess;
    public Button FirstNegMethodMore;
    public Button FirstNegMethodLess;
    public Button FirstNegMannerMore;
    public Button FirstNegMannerLess;
    public Button SecondAffMatterMore;
    public Button SecondAffMatterLess;
    public Button SecondAffMethodMore;
    public Button SecondAffMethodLess;
    public Button SecondAffMannerMore;
    public Button SecondAffMannerLess;
    public Button SecondNegMatterMore;
    public Button SecondNegMatterLess;
    public Button SecondNegMethodMore;
    public Button SecondNegMethodLess;
    public Button SecondNegMannerMore;
    public Button SecondNegMannerLess;
    public Button ThirdAffMatterMore;
    public Button ThirdAffMatterLess;
    public Button ThirdAffMethodMore;
    public Button ThirdAffMethodLess;
    public Button ThirdAffMannerMore;
    public Button ThirdAffMannerLess;
    public Button ThirdNegMatterMore;
    public Button ThirdNegMatterLess;
    public Button ThirdNegMethodMore;
    public Button ThirdNegMethodLess;
    public Button ThirdNegMannerMore;
    public Button ThirdNegMannerLess;

    // Start with scores of "0", change from there (retrieve previous scores from SharedPreferences, if possible, so we don't loose everything when the tab changes)
    int FirstAffMatterCounter = 0;
    int FirstAffMethodCounter = 0;
    int FirstAffMannerCounter = 0;
    int FirstNegMatterCounter = 0;
    int FirstNegMethodCounter = 0;
    int FirstNegMannerCounter = 0;
    int SecondAffMatterCounter = 0;
    int SecondAffMethodCounter = 0;
    int SecondAffMannerCounter = 0;
    int SecondNegMatterCounter = 0;
    int SecondNegMethodCounter = 0;
    int SecondNegMannerCounter = 0;
    int ThirdAffMatterCounter = 0;
    int ThirdAffMethodCounter = 0;
    int ThirdAffMannerCounter = 0;
    int ThirdNegMatterCounter = 0;
    int ThirdNegMethodCounter = 0;
    int ThirdNegMannerCounter = 0;

    // Open up SharedPreferences so that we can use it
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the package name and put it in a string, as .getPackageName() can't be called in a private method as needed in changeImage
        PackageName = getActivity().getApplicationContext().getPackageName();

        // For when we switch back from another fragment, so that what is displayed and what is stored in memory agree with each other, write memory to the screen
        // (as of version 2.0 this is now achieved using changeImage(), to transition to images, so this basically calls all images to update based on current values)

    }

    // When the tab is "paused", save everything so it can be used in FragmentScores.java, and so we don't lose anything when the app changes fragments or closes.
    public void onPause() {
        super.onPause();
        // Open SharedPreferences file to save everything
        SharedPreferences sharedPref = getActivity().getSharedPreferences("com.independent.dav.scores", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        // Save current value in the counter(s) to their corresponding location in SharedPreferences
        editor.putInt("FirstAffMatterStorage", FirstAffMatterCounter);
        editor.putInt("FirstAffMethodStorage", FirstAffMethodCounter);
        editor.putInt("FirstAffMannerStorage", FirstAffMannerCounter);
        editor.putInt("FirstNegMatterStorage", FirstNegMatterCounter);
        editor.putInt("FirstNegMethodStorage", FirstNegMethodCounter);
        editor.putInt("FirstNegMannerStorage", FirstNegMannerCounter);
        editor.putInt("SecondAffMatterStorage", SecondAffMatterCounter);
        editor.putInt("SecondAffMethodStorage", SecondAffMethodCounter);
        editor.putInt("SecondAffMannerStorage", SecondAffMannerCounter);
        editor.putInt("SecondNegMatterStorage", SecondNegMatterCounter);
        editor.putInt("SecondNegMethodStorage", SecondNegMethodCounter);
        editor.putInt("SecondNegMannerStorage", SecondNegMannerCounter);
        editor.putInt("ThirdAffMatterStorage", ThirdAffMatterCounter);
        editor.putInt("ThirdAffMethodStorage", ThirdAffMethodCounter);
        editor.putInt("ThirdAffMannerStorage", ThirdAffMannerCounter);
        editor.putInt("ThirdNegMatterStorage", ThirdNegMatterCounter);
        editor.putInt("ThirdNegMethodStorage", ThirdNegMethodCounter);
        editor.putInt("ThirdNegMannerStorage", ThirdNegMannerCounter);
        // Save everything (in the background, otherwise we would use commit();)
        editor.apply();
    }

    public void onResume() {
        super.onResume();
        // all of these changeImage()'s were called in onCreateView, right after getting the values from sharedPreferences.
        // But it crashed, horribly, on startup... but not when changeImage was called from a button-press, interestingly enough.
        // Long story short, they have been moved to onResume(), because it isn't crashing.
        // We don't know why that is, but we aren't questioning it, we are just leaving it do it's thing.
        changeImage(FirstAffMatterCounter, "FirstAffMatterVisual");
        changeImage(FirstAffMethodCounter, "FirstAffMethodVisual");
        changeImage(FirstAffMannerCounter, "FirstAffMannerVisual");
        changeImage(FirstNegMatterCounter, "FirstNegMatterVisual");
        changeImage(FirstNegMethodCounter, "FirstNegMethodVisual");
        changeImage(FirstNegMannerCounter, "FirstNegMannerVisual");
        changeImage(SecondAffMatterCounter, "SecondAffMatterVisual");
        changeImage(SecondAffMethodCounter, "SecondAffMethodVisual");
        changeImage(SecondAffMannerCounter, "SecondAffMannerVisual");
        changeImage(SecondNegMatterCounter, "SecondNegMatterVisual");
        changeImage(SecondNegMethodCounter, "SecondNegMethodVisual");
        changeImage(SecondNegMannerCounter, "SecondNegMannerVisual");
        changeImage(ThirdAffMatterCounter, "ThirdAffMatterVisual");
        changeImage(ThirdAffMethodCounter, "ThirdAffMethodVisual");
        changeImage(ThirdAffMannerCounter, "ThirdAffMannerVisual");
        changeImage(ThirdNegMatterCounter, "ThirdNegMatterVisual");
        changeImage(ThirdNegMethodCounter, "ThirdNegMethodVisual");
        changeImage(ThirdNegMannerCounter, "ThirdNegMannerVisual");

    }

    public void changeImage(int score, String speaker) {

        // Correlate the speaker given in the method call to the ImageView that represents it
        Output = (ImageView) getActivity().findViewById(getResources().getIdentifier(speaker, "id", PackageName));

        // Change the icon that represents score, based on the score given in the method call
        if (score == 1) {
            Output.setImageResource(R.drawable.ic_score_1_more_24dp);
            Output.setContentDescription(getString(R.string.average_1_more));
        } else if (score == 2) {
            Output.setImageResource(R.drawable.ic_score_2_more_24dp);
            Output.setContentDescription(getString(R.string.average_2_more));
        } else if (score == 3) {
            Output.setImageResource(R.drawable.ic_score_3_more_24dp);
            Output.setContentDescription(getString(R.string.average_3_more));
        } else if (score == 4) {
            Output.setImageResource(R.drawable.ic_score_4_more_24dp);
            Output.setContentDescription(getString(R.string.average_4_more));
        } else if (score == 0) {
            Output.setImageResource(R.drawable.ic_score_0_none_24dp);
            Output.setContentDescription(getString(R.string.average_0_none));
        } else if (score == -4) {
            Output.setImageResource(R.drawable.ic_score_4_less_24dp);
            Output.setContentDescription(getString(R.string.average_4_less));
        } else if (score == -3) {
            Output.setImageResource(R.drawable.ic_score_3_less_24dp);
            Output.setContentDescription(getString(R.string.average_3_less));
        } else if (score == -2) {
            Output.setImageResource(R.drawable.ic_score_2_less_24dp);
            Output.setContentDescription(getString(R.string.average_2_less));
        } else if (score == -1) {
            Output.setImageResource(R.drawable.ic_score_1_less_24dp);
            Output.setContentDescription(getString(R.string.average_1_less));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // This is the magic ingredient that makes the assigning of fields work, without it this code only works in Activities, not fragments
        View rootView = inflater.inflate(R.layout.fragment_speakers, container, false);

        // Setup SharedPreferences to start sharing stuff for us to use
        SharedPreferences sharedPref = getActivity().getSharedPreferences("com.independent.dav.scores", Context.MODE_PRIVATE);

        // Get saved values from SharedPreferences
        FirstAffMatterCounter = sharedPref.getInt("FirstAffMatterStorage", 0);
        FirstAffMethodCounter = sharedPref.getInt("FirstAffMethodStorage", 0);
        FirstAffMannerCounter = sharedPref.getInt("FirstAffMannerStorage", 0);
        FirstNegMatterCounter = sharedPref.getInt("FirstNegMatterStorage", 0);
        FirstNegMethodCounter = sharedPref.getInt("FirstNegMethodStorage", 0);
        FirstNegMannerCounter = sharedPref.getInt("FirstNegMannerStorage", 0);
        SecondAffMatterCounter = sharedPref.getInt("SecondAffMatterStorage", 0);
        SecondAffMethodCounter = sharedPref.getInt("SecondAffMethodStorage", 0);
        SecondAffMannerCounter = sharedPref.getInt("SecondAffMannerStorage", 0);
        SecondNegMatterCounter = sharedPref.getInt("SecondNegMatterStorage", 0);
        SecondNegMethodCounter = sharedPref.getInt("SecondNegMethodStorage", 0);
        SecondNegMannerCounter = sharedPref.getInt("SecondNegMannerStorage", 0);
        ThirdAffMatterCounter = sharedPref.getInt("ThirdAffMatterStorage", 0);
        ThirdAffMethodCounter = sharedPref.getInt("ThirdAffMethodStorage", 0);
        ThirdAffMannerCounter = sharedPref.getInt("ThirdAffMannerStorage", 0);
        ThirdNegMatterCounter = sharedPref.getInt("ThirdNegMatterStorage", 0);
        ThirdNegMethodCounter = sharedPref.getInt("ThirdNegMethodStorage", 0);
        ThirdNegMannerCounter = sharedPref.getInt("ThirdNegMannerStorage", 0);

        // public void onTrickyBusiness("SHIT-GONNA-GET-REAL", now)
        // The following is repeated for every button in the layout:
        // Start listening for button press. If pressed, check if changing the value will make it out-of-bounds.
        // If it won't, change the value and write it to memory, then display the new value in the text box (not in that order).
        FirstAffMatterMore = (Button) rootView.findViewById(R.id.FirstAffMatterMore);
        FirstAffMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(FirstAffMatterCounter < 4){
                    FirstAffMatterCounter++;
                    changeImage(FirstAffMatterCounter, "FirstAffMatterVisual");
                }
            }
        });
        FirstAffMatterLess = (Button) rootView.findViewById(R.id.FirstAffMatterLess);
        FirstAffMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstAffMatterCounter > -4){
                    FirstAffMatterCounter--;
                    changeImage(FirstAffMatterCounter, "FirstAffMatterVisual");
                }
            }
        });
        FirstAffMethodMore = (Button) rootView.findViewById(R.id.FirstAffMethodMore);
        FirstAffMethodMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view){
                if(FirstAffMethodCounter < 2){
                    FirstAffMethodCounter++;
                    changeImage(FirstAffMethodCounter, "FirstAffMethodVisual");
                }
            }
        });
        FirstAffMethodLess = (Button) rootView.findViewById(R.id.FirstAffMethodLess);
        FirstAffMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstAffMethodCounter > -2){
                    FirstAffMethodCounter--;
                    changeImage(FirstAffMethodCounter, "FirstAffMethodVisual");
                }
            }
        });
        FirstAffMannerMore = (Button) rootView.findViewById(R.id.FirstAffMannerMore);
        FirstAffMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(FirstAffMannerCounter < 4){
                    FirstAffMannerCounter++;
                    changeImage(FirstAffMannerCounter, "FirstAffMannerVisual");
                }
            }
        });
        FirstAffMannerLess = (Button) rootView.findViewById(R.id.FirstAffMannerLess);
        FirstAffMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstAffMannerCounter > -4){
                    FirstAffMannerCounter--;
                    changeImage(FirstAffMannerCounter, "FirstAffMannerVisual");
                }
            }
        });
        FirstNegMatterMore = (Button) rootView.findViewById(R.id.FirstNegMatterMore);
        FirstNegMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(FirstNegMatterCounter < 4){
                    FirstNegMatterCounter++;
                    changeImage(FirstNegMatterCounter, "FirstNegMatterVisual");
                }
            }
        });
        FirstNegMatterLess = (Button) rootView.findViewById(R.id.FirstNegMatterLess);
        FirstNegMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstNegMatterCounter > -4){
                    FirstNegMatterCounter--;
                    changeImage(FirstNegMatterCounter, "FirstNegMatterVisual");
                }
            }
        });
        FirstNegMethodMore = (Button) rootView.findViewById(R.id.FirstNegMethodMore);
        FirstNegMethodMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(FirstNegMethodCounter < 2){
                    FirstNegMethodCounter++;
                    changeImage(FirstNegMethodCounter, "FirstNegMethodVisual");
                }
            }
        });
        FirstNegMethodLess = (Button) rootView.findViewById(R.id.FirstNegMethodLess);
        FirstNegMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstNegMethodCounter > -2){
                    FirstNegMethodCounter--;
                    changeImage(FirstNegMethodCounter, "FirstNegMethodVisual");
                }
            }
        });
        FirstNegMannerMore = (Button) rootView.findViewById(R.id.FirstNegMannerMore);
        FirstNegMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(FirstNegMannerCounter < 4){
                    FirstNegMannerCounter++;
                    changeImage(FirstNegMannerCounter, "FirstNegMannerVisual");
                }
            }
        });
        FirstNegMannerLess = (Button) rootView.findViewById(R.id.FirstNegMannerLess);
        FirstNegMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstNegMannerCounter > -4){
                    FirstNegMannerCounter--;
                    changeImage(FirstNegMannerCounter, "FirstNegMannerVisual");
                }
            }
        });
        SecondAffMatterMore = (Button) rootView.findViewById(R.id.SecondAffMatterMore);
        SecondAffMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondAffMatterCounter < 4){
                    SecondAffMatterCounter++;
                    changeImage(SecondAffMatterCounter, "SecondAffMatterVisual");
                }
            }
        });
        SecondAffMatterLess = (Button) rootView.findViewById(R.id.SecondAffMatterLess);
        SecondAffMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondAffMatterCounter > -4){
                    SecondAffMatterCounter--;
                    changeImage(SecondAffMatterCounter, "SecondAffMatterVisual");
                }
            }
        });
        SecondAffMethodMore = (Button) rootView.findViewById(R.id.SecondAffMethodMore);
        SecondAffMethodMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondAffMethodCounter < 2){
                    SecondAffMethodCounter++;
                    changeImage(SecondAffMethodCounter, "SecondAffMethodVisual");
                }
            }
        });
        SecondAffMethodLess = (Button) rootView.findViewById(R.id.SecondAffMethodLess);
        SecondAffMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondAffMethodCounter > -2){
                    SecondAffMethodCounter--;
                    changeImage(SecondAffMethodCounter, "SecondAffMethodVisual");
                }
            }
        });
        SecondAffMannerMore = (Button) rootView.findViewById(R.id.SecondAffMannerMore);
        SecondAffMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondAffMannerCounter < 4){
                    SecondAffMannerCounter++;
                    changeImage(SecondAffMannerCounter, "SecondAffMannerVisual");
                }
            }
        });
        SecondAffMannerLess = (Button) rootView.findViewById(R.id.SecondAffMannerLess);
        SecondAffMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondAffMannerCounter > -4){
                    SecondAffMannerCounter--;
                    changeImage(SecondAffMannerCounter, "SecondAffMannerVisual");
                }
            }
        });
        SecondNegMatterMore = (Button) rootView.findViewById(R.id.SecondNegMatterMore);
        SecondNegMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondNegMatterCounter < 4){
                    SecondNegMatterCounter++;
                    changeImage(SecondNegMatterCounter, "SecondNegMatterVisual");
                }
            }
        });
        SecondNegMatterLess = (Button) rootView.findViewById(R.id.SecondNegMatterLess);
        SecondNegMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondNegMatterCounter > -4){
                    SecondNegMatterCounter--;
                    changeImage(SecondNegMatterCounter, "SecondNegMatterVisual");
                }
            }
        });
        SecondNegMethodMore = (Button) rootView.findViewById(R.id.SecondNegMethodMore);
        SecondNegMethodMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondNegMethodCounter < 2){
                    SecondNegMethodCounter++;
                    changeImage(SecondNegMethodCounter, "SecondNegMethodVisual");
                }
            }
        });
        SecondNegMethodLess = (Button) rootView.findViewById(R.id.SecondNegMethodLess);
        SecondNegMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondNegMethodCounter > -2){
                    SecondNegMethodCounter--;
                    changeImage(SecondNegMethodCounter, "SecondNegMethodVisual");
                }
            }
        });
        SecondNegMannerMore = (Button) rootView.findViewById(R.id.SecondNegMannerMore);
        SecondNegMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(SecondNegMannerCounter < 4){
                    SecondNegMannerCounter++;
                    changeImage(SecondNegMannerCounter, "SecondNegMannerVisual");
                }
            }
        });
        SecondNegMannerLess = (Button) rootView.findViewById(R.id.SecondNegMannerLess);
        SecondNegMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SecondNegMannerCounter > -4){
                    SecondNegMannerCounter--;
                    changeImage(SecondNegMannerCounter, "SecondNegMannerVisual");
                }
            }
        });
        ThirdAffMatterMore = (Button) rootView.findViewById(R.id.ThirdAffMatterMore);
        ThirdAffMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdAffMatterCounter < 4){
                    ThirdAffMatterCounter++;
                    changeImage(ThirdAffMatterCounter, "ThirdAffMatterVisual");
                }
            }
        });
        ThirdAffMatterLess = (Button) rootView.findViewById(R.id.ThirdAffMatterLess);
        ThirdAffMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdAffMatterCounter > -4){
                    ThirdAffMatterCounter--;
                    changeImage(ThirdAffMatterCounter, "ThirdAffMatterVisual");
                }
            }
        });
        ThirdAffMethodMore = (Button) rootView.findViewById(R.id.ThirdAffMethodMore);
        ThirdAffMethodMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdAffMethodCounter < 2){
                    ThirdAffMethodCounter++;
                    changeImage(ThirdAffMethodCounter, "ThirdAffMethodVisual");
                }
            }
        });
        ThirdAffMethodLess = (Button) rootView.findViewById(R.id.ThirdAffMethodLess);
        ThirdAffMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdAffMethodCounter > -2){
                    ThirdAffMethodCounter--;
                    changeImage(ThirdAffMethodCounter, "ThirdAffMethodVisual");
                }
            }
        });
        ThirdAffMannerMore = (Button) rootView.findViewById(R.id.ThirdAffMannerMore);
        ThirdAffMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdAffMannerCounter < 4){
                    ThirdAffMannerCounter++;
                    changeImage(ThirdAffMannerCounter, "ThirdAffMannerVisual");
                }
            }
        });
        ThirdAffMannerLess = (Button) rootView.findViewById(R.id.ThirdAffMannerLess);
        ThirdAffMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdAffMannerCounter > -4){
                    ThirdAffMannerCounter--;
                    changeImage(ThirdAffMannerCounter, "ThirdAffMannerVisual");
                }
            }
        });
        ThirdNegMatterMore = (Button) rootView.findViewById(R.id.ThirdNegMatterMore);
        ThirdNegMatterMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdNegMatterCounter < 4){
                    ThirdNegMatterCounter++;
                    changeImage(ThirdNegMatterCounter, "ThirdNegMatterVisual");
                }
            }
        });
        ThirdNegMatterLess = (Button) rootView.findViewById(R.id.ThirdNegMatterLess);
        ThirdNegMatterLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdNegMatterCounter > -4){
                    ThirdNegMatterCounter--;
                    changeImage(ThirdNegMatterCounter, "ThirdNegMatterVisual");
                }
            }
        });
        ThirdNegMethodMore = (Button) rootView.findViewById(R.id.ThirdNegMethodMore);
        ThirdNegMethodMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdNegMethodCounter < 2){
                    ThirdNegMethodCounter++;
                    changeImage(ThirdNegMethodCounter, "ThirdNegMethodVisual");
                }
            }
        });
        ThirdNegMethodLess = (Button) rootView.findViewById(R.id.ThirdNegMethodLess);
        ThirdNegMethodLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdNegMethodCounter > -2){
                    ThirdNegMethodCounter--;
                    changeImage(ThirdNegMethodCounter, "ThirdNegMethodVisual");
                }
            }
        });
        ThirdNegMannerMore = (Button) rootView.findViewById(R.id.ThirdNegMannerMore);
        ThirdNegMannerMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if(ThirdNegMannerCounter < 4){
                    ThirdNegMannerCounter++;
                    changeImage(ThirdNegMannerCounter, "ThirdNegMannerVisual");
                }
            }
        });
        ThirdNegMannerLess = (Button) rootView.findViewById(R.id.ThirdNegMannerLess);
        ThirdNegMannerLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ThirdNegMannerCounter > -4){
                    ThirdNegMannerCounter--;
                    changeImage(ThirdNegMannerCounter, "ThirdNegMannerVisual");
                }
            }
        });

        // Actually show the changes on the screen, instead of just thinking about them.
        //changeImage(FirstAffMatterCounter,"FirstAffMatterVisual"); ##TESTING/DE-BUGGING USE
        return rootView;
    }
}